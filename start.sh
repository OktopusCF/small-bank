#!/bin/bash
if hash composer; then
    echo "Install dependencies"
    composer install
    echo "Create migrations..."
    php artisan migrate
    echo "Create seeds..."
    php artisan db:seed
    echo "Start working day..."
    php artisan day:start
    echo "Start queue..."
    php artisan queue:work --queue=high,low
fi
echo "Please, install Composer"
exit