document.addEventListener('DOMContentLoaded', function () {
    pullGlobalVariable();
    reloadWalletsTable();
    loadTransferPage();
    loadTransactionPage();
    loadRates();
    $(function () {
        $("#date").datepicker({
            maxDate: 0,
            dateFormat: "yy-mm-dd"
        });
    });
    $(function () {
        var from = $("#transaction-from-date")
                .datepicker({
                    changeMonth: true,
                    dateFormat: "yy-mm-dd",
                    numberOfMonths: 2,
                    maxDate: '-7',

                })
                .on("change", function () {
                    to.datepicker("option", "minDate", getDate(this));
                }),
            to = $("#transaction-to-date").datepicker({
                changeMonth: true,
                dateFormat: "yy-mm-dd",
                numberOfMonths: 2,
                maxDate: 0,

            })
                .on("change", function () {
                    from.datepicker("option", "maxDate", getDate(this));
                });

        function getDate(element) {
            var date;
            try {
                date = $.datepicker.parseDate("yy-mm-dd", element.value);
            } catch (error) {
                date = null;
            }

            return date;
        }
    });
}, false);

function downloadTransaction() {
    let from = document.getElementById('transaction-from-date').value,
        to = document.getElementById('transaction-to-date').value,
        wallet_no = document.getElementById('transaction-form-select').value;
    axios.get("/api/transaction/export/" +
        user.name + "/" +
        wallet_no + "/" +
        from + "/" +
        to + "/?api_token=" + api_token, {
        responseType: 'arraybuffer',
    })
        .then(function (response) {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'file.xls');
            document.body.appendChild(link);
            link.click();
            // var blob = new Blob([response], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
            // saveAs(blob, title + ".xlsx");
        })
        .catch(function (error) {
            console.log(error);
            alert(error.message)

        });
}

function loadTransactionPage() {
    let tmp = document.getElementById('transaction-table-template').innerText,
        from = document.getElementById('transaction-from-date').value,
        to = document.getElementById('transaction-to-date').value,
        wallet_no = document.getElementById('transaction-form-select').value;

    axios.get("/api/transaction/" +
        user.name + "/" +
        wallet_no + "/" +
        from + "/" +
        to + "/?api_token=" + api_token)
        .then(function (response) {
            transactions = response.data.data.data;
            document.getElementById('transaction-wrapper').innerHTML = templateEngine(tmp, transactions);
        })
        .catch(function (error) {
            console.log(error);
            alert(error.message)

        });
    // document.getElementById('transaction-wrapper').innerHTML = templateEngine(tmp, transactions);

}

var transfer_hash;

function loadRates() {
    let defaultCurrency = document.getElementById('default-currency');
    defaultCurrency.innerText = rates.base
    let wrapper = document.getElementById('rate-wrapper'),
        template = document.getElementById('rates-table-template').innerText
    wrapper.innerHTML = templateEngine(template, rates);
    $('#currencies-rate-table').DataTable();
    document.querySelector('#currencies-rate-table_length select[name=currencies-rate-table_length]').classList.add('form-control')
    document.querySelector('#currencies-rate-table_filter input[type=search]').classList.add('form-control')

}

function donateWallet(that) {
    let wallet_no = that.dataset.walletNo;
    console.log('wallet', wallet_no);
    axios.post("/api/deposit/"+wallet_no,
        {
            value: document.getElementById('donate-input-'+wallet_no).value,
        })
        .then(function (response) {
            console.log(response);
            refreshWallets()
        })
        .catch(function (error) {
            console.log(error);
            alert(error.message)

        });
}

function createWallet() {
    let modalDismissBtn = document.getElementById('modal-dismiss-btn');
    axios.post("/api/wallet?api_token=" + api_token,
        {
            name: document.getElementById('name').value,
            currency_code: document.getElementById('currency').value,
        })
        .then(function (response) {
            wallets[wallets.length] = response.data.data.wallet
            reloadWalletsTable()
        })
        .catch(function (error) {
            console.log(error);
            alert(error.message)

        });
    modalDismissBtn.click()
}

function resetCreateWalletDialog() {
    document.getElementById('name').value = '';
    document.getElementById('currency').selectedIndex = 0;
}

function loadTransferPage() {
    let wrapper = document.getElementById('transfer'),
        template = document.getElementById('transfer-template').innerText
    wrapper.innerHTML = templateEngine(template, wallets);
    // wrapper.querySelector('#donor-detail').innerHTML = document.getElementById('donor-details-template').innerText
    // wrapper.querySelector('#acceptor-detail').innerHTML = document.getElementById('acceptor-details-template').innerText
}

function transferStep2() {
    let transferFormStepOne = document.getElementById('transfer-form')
    let data = {
        donor_wallet_no: transferFormStepOne.querySelector("#transfer-form-donor-select").value,
        acceptor_wallet_no: transferFormStepOne.querySelector("#transfer-form-acceptor-input").value.replace(/\s/g, ''),
        is_acceptor_currency: transferFormStepOne.querySelector("#transfer-form-acceptor-currency").checked,
        amount: transferFormStepOne.querySelector("#transfer-form-amount").value,
        description: transferFormStepOne.querySelector("#description-input").value,
    }
    axios.post("/api/transfer/step-one?api_token=" + api_token, data)
        .then(function (response) {
            if (response.data.success) {
                let template = document.getElementById('details-template').innerText;
                let wrapper = document.getElementById('details-wrapper')
                wrapper.innerHTML = detailsTmp(response.data.data);
                toNextStep();
                transfer_hash = response.data.data.unique_hash;
            } else if (response.data.message) {
                alert(response.data.message)
            }
        })
        .catch(function (error) {
            console.log('error', error);
            alert(error.message)

        });
}

function toPrevStep() {
    document.getElementById('transfer-details').style.display = 'none';
    document.getElementById('transfer-prev-btn').style.display = 'none';
    document.getElementById('transfer-go-btn').style.display = 'none';
    document.getElementById('transfer-reset-btn').style.display = 'inline-block';
    document.getElementById('transfer-next-btn').style.display = 'inline-block';
    document.getElementById('transfer-form-donor-select').disabled = false;
    document.getElementById('transfer-form-acceptor-input').disabled = false;
    document.getElementById('transfer-form-acceptor-currency').disabled = false;
    document.getElementById('transfer-form-amount').disabled = false;

}

function toNextStep() {
    document.getElementById('transfer-details').style.display = 'block';
    document.getElementById('transfer-prev-btn').style.display = 'inline-block';
    document.getElementById('transfer-go-btn').style.display = 'inline-block';
    document.getElementById('transfer-reset-btn').style.display = 'none';
    document.getElementById('transfer-next-btn').style.display = 'none';
    document.getElementById('transfer-form-donor-select').disabled = true;
    document.getElementById('transfer-form-acceptor-input').disabled = true;
    document.getElementById('transfer-form-acceptor-currency').disabled = true;
    document.getElementById('transfer-form-amount').disabled = true;
}


function transferGo() {
    axios.post("/api/transfer/step-two?api_token=" + api_token, {
        unique_hash: transfer_hash
    })
        .then(function (response) {
            if (response.data.success) {
                document.getElementById('transfer-details').style.display = 'none';
                // document.getElementById('transfer-complete').style.display = "block"
                document.getElementById('transfer-prev-btn').click();
                document.getElementById('transfer-reset-btn').click();
                alert(response.data.data.message)

                // document.getElementById('transfer-message').innerText = response.data.data.message;
                refreshWallets()
                loadTransferPage()
            } else if (response.data.message) {
                alert(response.data.message)
            }
        })
        .catch(function (error, response) {
            console.log('error', error);
            alert(error.message)

        });

}

function closeWallet(that) {
    let re = /-[0-9]*-/g
    let walletId = re.exec(that.id)[0].slice(1, -1)
    let wallet = wallets.filter(wallet => wallet.id == walletId)[0]
    if (wallet['value'] !== 0) {
        alert("Wallet " + wallet.name + " can't be closed. It's not empty.")
        return
    }
    axios.delete("/api/wallet/" + walletId + "?api_token=" + api_token)
        .then(function (response) {
            wallets = wallets.filter(wallet => wallet.id != walletId)
            reloadWalletsTable()
        })
        .catch(function (error) {
            console.log(error);
            alert(error.message)
        });
}

function refreshWallets() {
    setTimeout(() => {
        axios.get("/api/wallet/?api_token=" + api_token)
            .then(function (response) {
                wallets = response.data.data.wallets
                reloadWalletsTable()
                loadTransferPage()
            })
            .catch(function (error) {
                console.log(error);
                alert(error.message)
            });

    }, 3000)
}

function reloadWalletsTable() {
    let wrapper = document.getElementById('wallet-content'),
        template = document.getElementById('wallets-table-template').innerText
    wrapper.innerHTML = templateEngine(template, wallets);
    $('#wallets-list').DataTable();
    document.querySelector('#wallet-content input[type=search]').classList.add('form-control')
    document.querySelector('#wallet-content select[name=wallets-list_length]').classList.add('form-control')
}

/**********************************************/
/*              HELPERS FUNCTIONS             */

/**********************************************/

function reloadRates(date) {
    axios({
        method: 'get',
        url: "https://ratesapi.io/api/" + date + "?base=USD",
    })
        .then(function (response) {
            rates = response.data
            loadRates()
        })
        .catch(function (error) {
            console.log(error);
            alert(error.message)
        });
}

function currencyFormat(currency, code) {
    let result = new Intl.NumberFormat('ru-RU', {
        useGrouping: true,
        maximumFractionDigits: 5,
        style: 'currency',
        currency: code
    }).format(currency);
    return result
}

function detailsTmp(data) {
    {
        var r = [];
        r.push("     <div class='col-md-6' id='donor-detail'>         <div>Donor name: <b>");
        r.push(data.donor.user.name);
        r.push("</b></div>         <div>Donor email: <b>");
        r.push(data.donor.user.email);
        r.push("</b></div>         <div>Donor wallet unique No: <b>");
        r.push(data.donor.wallet_no.replace(/(\d)(?=(\d\d\d\d)+([^\d]|$))/g, '$1 '));
        r.push("</b></div>         <div>Donor Currency code: <b>");
        r.push(data.donor.currency_code);
        r.push("</b></div>         <div>Donor charge amount: <b>");
        r.push(data.donor_amount);
        r.push("</b></div>     </div>     <div class='col-md-6' id='acceptor-detail'>         <div>Acceptor name: <b>");
        r.push(data.acceptor.user.name);
        r.push("</b></div>         <div>Acceptor email: <b>");
        r.push(data.acceptor.user.email);
        r.push("</b></div>         <div>Acceptor wallet unique No: <b>");
        r.push(data.acceptor.wallet_no.replace(/(\d)(?=(\d\d\d\d)+([^\d]|$))/g, '$1 '));
        r.push("</b></div>         <div>Acceptor Currency code: <b>");
        r.push(data.acceptor.currency_code);
        r.push(" </b></div>         <div>Acceptor charge amount: <b>");
        r.push(data.acceptor_amount);
        r.push("</b></div>     </div> ");
        return r.join("");
    }
}

function templateEngine(html, options) {
    var re = /<%([^%>]+)?%>/g,
        reExp = /(^( )?(if|for|else|switch|case|break|var|{|}))(.*)?/g,
        code = '{var r=[];\n',
        cursor = 0,
        match;
    var add = function (line, js) {
        js ? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
            (code += (line != '' || line !== null) ? 'r.push("' + line + '");\n' : '');
        return add;
    }
    while (match = re.exec(html)) {
        add(html.slice(cursor, match.index))(match[1], true);
        cursor = match.index + match[0].length;
    }
    add(html.substr(cursor, html.length - cursor));
    code = (code + 'return r.join(""); }').replace(/[\r\t\n]/g, ' ');
    return new Function(code).apply(options);
}