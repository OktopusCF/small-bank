<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/rates/{date?}', 'ApiController@getRates');

Route::post('/register', 'Auth\RegisterController@register');
Route::post('/deposit/{wallet_no}', 'ApiController@deposit');


Route::middleware('auth:api')->group(function (){
    Route::resource('/wallet', 'WalletController');

    Route::get('transaction/{name}/{wallet}/{from?}/{to?}', 'ApiController@getTransaction');
    Route::get('transaction/export/{name}/{wallet}/{from?}/{to?}', 'ApiController@exportTransaction');

    Route::post('transfer/step-one', 'ApiController@transferStepOne');
    Route::post('transfer/step-two', 'ApiController@transferStepTwo');


});


