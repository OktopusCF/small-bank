@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link" id="wallets-tab" data-toggle="tab" href="#wallets" role="tab"
                           aria-controls="wallets" aria-selected="true">My wallets</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" id="transfer-tab" data-toggle="tab" href="#transfer" role="tab"
                           aria-controls="transfer" aria-selected="false">Transfer</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="transaction-tab" data-toggle="tab" href="#transaction" role="tab"
                           aria-controls="transaction" aria-selected="false">Transaction</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="rates-tab" data-toggle="tab" href="#rates" role="tab"
                           aria-controls="rates" aria-selected="false">Exchange Rate</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="wallets" role="tabpanel" aria-labelledby="wallets-tab">
                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#newWalletModal"
                                style="margin: 10px 0 -10px 10px">
                            Create new wallet ...
                        </button>
                        <hr>
                        <div class="container-fluid" id="wallet-content">
                        </div>
                    </div>
                    <div class="tab-pane " id="transfer" role="tabpanel" aria-labelledby="transfer-tab">
                    </div>
                    <div class="tab-pane" id="transaction" role="tabpanel" aria-labelledby="transaction-tab">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-xs-1">
                                    <label for="transaction-from-date" class="col-md-4 control-label"
                                           style="margin-top: 10px;width: auto">From</label>
                                    <input id="transaction-from-date" type="text" class="form-control"
                                           name="transaction-from-date" onchange='loadTransactionPage()'
                                           value={!! \Carbon\Carbon::now()->startOfMonth() !!} >
                                </div>
                                <div class="col-md-6 col-xs-1">
                                    <label for="transaction-to-date" class="col-md-4 control-label"
                                           style="margin-top: 10px;width: auto">To</label>
                                    <input id="transaction-to-date" type="text" class="form-control"
                                           name="transaction-to-date" onchange='loadTransactionPage()'
                                           value={!! \Carbon\Carbon::today() !!} >
                                </div>
                            </div>
                            <div class='form-group'>
                                <label for='transaction-select' class='col-md-5 control-label'>Select your
                                    Wallet</label>
                                <select class='form-control' id='transaction-form-select'
                                        onchange='loadTransactionPage()' required>
                                    @foreach($wallets as $wallet)
                                        <option value={{$wallet->wallet_no}}>
                                            {{$wallet->wallet_no}} - {{$wallet->value}} {{$wallet->currency_code}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div id="transaction-wrapper" style="padding: 10px">

                            </div>
                            <div>
                                <button onclick="downloadTransaction()">Download XLS format</button>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane" id="rates" role="tabpanel" aria-labelledby="rates-tab">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-xs-1">
                                    <label for="date" class="col-md-4 control-label"
                                           style="margin-top: 10px;width: auto">Rate date</label>
                                    <input id="date" type="text" class="form-control" name="date"
                                           onchange="reloadRates(this.value)" value={!! \Carbon\Carbon::today() !!} >
                                </div>
                                <div class="col-md-6 col-xs-1">
                                    <p style="margin: 10px"> Default currency: <b id="default-currency"></b></p>
                                </div>
                            </div>
                            <div id="rate-wrapper" style="padding: 10px">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="newWalletModal" tabindex="-1" role="dialog" aria-labelledby="newWalletModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title control-label" id="newWalletModalLabel">Create new Wallet</h5>
                </div>
                <form>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <label for="name" class="col-md-4 control-label">Wallet's name</label>
                            <input id="name" type="text" class="form-control" name="name" required>
                        </div>
                        <div class="container-fluid">
                            <label for="currency" class="col-md-4 control-label">Wallet's currency</label>
                            <select class="form-control" id="currency">
                                <option disabled selected>select currency</option>
                                @foreach ($currencies as $curency)
                                    <option value="{{$curency['code']}}">{{$curency['code'] . ' - ' . $curency['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary" id="modal-dismiss-btn" data-dismiss="modal"
                            onclick="resetCreateWalletDialog()">Close
                    </button>
                    <button type="button" class="btn btn-primary" onclick="createWallet()">Create</button>
                </div>
            </div>
        </div>
    </div>
    <script type="application/javascript">
        function pullGlobalVariable() {
            wallets = {!! $wallets->toJson() !!};
            currencies = {!! $currencies->toJson() !!};
            user = {!! Auth::user() !!};
            rates = {!! json_encode($rates, true) !!};
            transactions = {!! $transactions->toJson() !!};
            sample = {!! $sample->toJson() !!};
            api_token = "{{ Auth::user()->api_token }}";
        }
    </script>
    <!-- Script templates -->
    <script type="text/x-custom-template" id="rates-table-template">
        <table class='table table-striped table-hover' id='currencies-rate-table'>
            <thead>
            <tr>
                <th width='50%'>Currency Code</th>
                <th width='50%'>Value</th>
            </tr>
            </tr>closeWallet(this)
            </thead>
            <tbody>
            <% for(let index in rates.rates){ %>
            <tr>
                <td><% index %></td>
                <td style='text-align: right;padding-right: 50px'><% currencyFormat(rates.rates[index], index) %></td>
            </tr>
            <% } %>
            </tbody>
        </table>
    </script>

    <script type="text/x-custom-template" id="transfer-template">
        <div class='container-fluid' style='padding: 20px'>
            <form id='transfer-form'>
                <div class='form-group'>
                    <label for='donor-select' class='col-md-5 control-label'>Select your Wallet</label>
                    <select class='form-control' id='transfer-form-donor-select' required>
                        <option disabled selected value=''>please select...</option>
                        <% for(let index in wallets){ %>
                        <option value='<% wallets[index].wallet_no %>'>
                            <% wallets[index].wallet_no.replace(/(\d)(?=(\d\d\d\d)+([^\d]|$))/g, '$1 ') %> - <%
                            currencyFormat(wallets[index].value, wallets[index].currency_code) %>
                        </option>
                        <% } %>
                    </select>
                </div>
                <div class='form-group'>
                    <label for='acceptor-input' class='col-md-5 control-label'>Input acceptor's wallet unique
                        No </label>
                    <input id='transfer-form-acceptor-input' type='text' class='form-control'
                           placeholder='Wallet unique No'
                           name='acceptor' value='' required>
                </div>
                <div class='form-group'>
                    <input class='form-check-input' type='checkbox' value='' id='transfer-form-acceptor-currency'
                           name='acceptor-currency'>
                    <label class='form-check-label' for='acceptor-currency'>
                        Transfer in the recipient’s account currency
                    </label>
                </div>
                <div class='form-group'>
                    <div class='input-group'>
                        <label for='amount' class='col-md-2 control-label'>Amount </label>
                        <span class='input-group-addon' id='basic-addon1'>@</span>
                        <input type='text' class='form-control' placeholder='0,00' id='transfer-form-amount'
                               aria-describedby='basic-addon1' required>
                    </div>
                </div>
                <div class='form-group'>
                    <label for='description-input' class='col-md-5 control-label'>Description </label>
                    <textarea id='description-input' class='form-control'
                           placeholder='Transfer of personal funds'
                              name='description' rows='2'></textarea>
                </div>
                <div class='form-group' id='transfer-details' style='display: none'>
                    <hr>
                    <p><h4>Transfer details</h4></p>
                    <div class='row' id='details-wrapper'>
                    </div>
                </div>
                <div id='transfer-complete' style='display: none'>
                    <h4 id='transfer-message'></h4>
                </div>
                <div class='form-group'>
                    <hr>
                    <button type='reset' class='btn btn-secondary' id='transfer-reset-btn'>Reset</button>
                    <button type='button' class='btn btn-primary' id='transfer-prev-btn' onclick='toPrevStep()'
                            style='display: none'><<< Prev
                        step
                    </button>
                    <button type='button' class='btn btn-primary' onclick='transferStep2()' id='transfer-next-btn'>Next
                        step
                        >>>
                    </button>
                    <button type='button' class='btn btn-warning' onclick='transferGo()' id='transfer-go-btn'
                            style='display: none'>Transfer!
                    </button>
                </div>
            </form>
            <div>
                <br>
                <hr>
                <h4>Exist Wallets list</h4>
                @foreach($sample as $wallet)
                    <p>Currency: <b>{{$wallet->currency_code}}</b> wallet No: <b>{{$wallet->wallet_no}}</b></p>
                @endforeach
            </div>
        </div>

    </script>


    <script type="text/x-custom-template" id="details-template">
        <div class='col-md-6' id='donor-detail'>
            <div>Donor name: <b><% response.data.data.donor.user.name %></b></div>
            <div>Donor email: <b><% response.data.data.donor.user.email %></b></div>
            <div>Donor wallet unique No: <b><%
                    response.data.data.donor.wallet_no.replace(/(\d)(?=(\d\d\d\d)+([^\d]|$))/g, '$1 ') %></b></div>
            <div>Donor Currency code: <b><% response.data.data.donor.currency_code %></b></div>
            <div>Donor charge amount: <b><% response.data.data.donor_amount.toFixed(2) %></b></div>
        </div>
        <div class='col-md-6' id='acceptor-detail'>
            <div>Acceptor name: <b><% data.acceptor.user.name %></b></div>
            <div>Acceptor email: <b><% data.acceptor.user.email %></b></div>
            <div>Acceptor wallet unique No: <b><% data.acceptor.wallet_no.replace(/(\d)(?=(\d\d\d\d)+([^\d]|$))/g, '$1
                    ') %></b></div>
            <div>Acceptor Currency code: <b><% data.acceptor.currency_code %> </b></div>
            <div>Acceptor charge amount: <b><% data.acceptor_amount.toFixed(2) %></b></div>
        </div>
    </script>


    <script type="text/x-custom-template" id="transaction-table-template">
        <table class='table table-striped table-hover' id='transaction-list'>
            <thead>
            <tr>
                <th width='20%'>Date</th>
                <th width='20%'>Wallet</th>
                <th width='15%'>Amount</th>
                <th width='15%'>Amount in USD</th>
                <th width='30%'>Description</th>
            </tr>
            </thead>
            <tbody>
            <% var wallet_no = document.getElementById('transaction-form-select').value;
            console.log('Wallet NO:', wallet_no);%>
            <% for(let index in transactions){ %>
            <% if (transactions[index].donor.wallet_no == wallet_no) {%>
            <tr style='background-color:#FFE7E2'>
                <td><% transactions[index].created_at %></td>
                <td><% transactions[index].acceptor.wallet_no %></td>
                <td style='text-align: right'> - <% currencyFormat(transactions[index].donor_amount,
                    transactions[index].donor.currency_code) %>
                </td>
                <td style='text-align: right'> - <% currencyFormat(transactions[index].main_currency_amount.toFixed(2), 'USD')%>
                </td>
                <td><% transactions[index].description %></td>
            </tr>
            <% } else { %>
            <tr style='background-color:#F6FFE2'>
                <td><% transactions[index].created_at %></td>
                <td><% transactions[index].donor.wallet_no %></td>
                <td style='text-align: right'> <% currencyFormat(transactions[index].acceptor_amount,
                    transactions[index].acceptor.currency_code) %>
                </td>
                <td style='text-align: right'> <% transactions[index].acceptor.currency_code == 'USD' ?
                    currencyFormat(transactions[index].acceptor_amount.toFixed(2), 'USD')
                    : currencyFormat(transactions[index].donor_amount.toFixed(2), 'USD') %>
                </td>
                <td><% transactions[index].description %></td>
            </tr>
            <% } %>
            <% } %>
            </tbody>
        </table>

    </script>

    <script type="text/x-custom-template" id="wallets-table-template">
        <table class='table table-striped table-hover' id='wallets-list'>
            <thead>
            <tr>
                <th width='25%'>Wallet name</th>
                <th width='25%'>Wallet unique No</th>
                <th width='20%'>Value</th>
                <th width='30%'></th>
            </tr>
            </thead>
            <tbody>
            <% for(let index in wallets){ %>
            <tr>
                <td><% wallets[index].name %></td>
                <td><% wallets[index].wallet_no.replace(/(\d)(?=(\d\d\d\d)+([^\d]|$))/g, '$1 ') %></td>
                <td style='text-align: right'><% currencyFormat(wallets[index].value*1,
                    wallets[index].currency_code)%>
                </td>
                <td>
                    <div class='row'>
                        <div class='col-md-7'>
                            <div class='input-group'>
                                <input type='text' class='form-control' placeholder='0.00'
                                       id='donate-input-<%wallets[index].wallet_no%>'>
                                <span class='input-group-btn'>
                            <button class='btn btn-default' type='button' data-wallet-no='<%wallets[index].wallet_no%>'
                                    onclick='donateWallet(this)'>Donate</button>
                        </span>
                            </div>
                        </div>
                        <div class='col-md-5'>
                            <button type='button' class='btn btn-primary btn-sm'
                                    id='close-wallet-<%wallets[index].id%>-btn'
                                    onclick='closeWallet(this)'>Close wallet
                            </button>
                        </div>
                    </div>
                </td>
            </tr>
            <% } %>
            </tbody>
        </table>
    </script>

@endsection
