<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 21.03.19
 * Time: 19:44
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class RatesFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Rates';
    }
}