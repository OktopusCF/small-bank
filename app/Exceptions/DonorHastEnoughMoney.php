<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class DonorHastEnoughMoney extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = "Donor has't enough money";
        parent::__construct($message, 200, $previous);
    }
}
