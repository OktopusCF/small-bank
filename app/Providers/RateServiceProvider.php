<?php

namespace App\Providers;

use App\Resource\Rates;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class RateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('Rates', function()
        {
            return new Rates ();
        });
    }
}
