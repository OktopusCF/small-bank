<?php

namespace App\Providers;

use App\Models\Currency;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Rates;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('home', function ($view) {
            $view->with([
                'currencies'=> Currency::orderBy('code')->get(),
                'wallets' => auth()->user()->wallets,
                'user'=> \Auth::user(),
                'rates'=> Rates::getAllRates(),
                'transactions'=> Transaction::getTransactions(auth()->user()->name, auth()->user()->wallet[0]),
                'sample'    => Wallet::where('user_id', '<>', auth()->id())->orderBy('user_id')->get(),
            ]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
