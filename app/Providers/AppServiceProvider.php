<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Wallet;
use App\Observers\UserObserver;
use App\Observers\WalletObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Wallet::observe(WalletObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
