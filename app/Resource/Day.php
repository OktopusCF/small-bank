<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 25.03.19
 * Time: 8:41
 */

namespace App\Resource;


use App\Constants\AppConstants;
use App\Models\Account;
use App\Models\Currency;
use Carbon\Carbon;
use Redis;

class Day
{

    protected static $status = FALSE;

    public static function start()
    {
        $accounts = Account::where('day', Carbon::yesterday()->format('Y-m-d'))
            ->where('status', 'close')
            ->get();
        foreach ($accounts as $account) {
            $currency['amount'] = $account->amount;
            Redis::set($account->code . ':amount', $currency['amount']);
        }
        self::$status = TRUE;

    }

    public static function stop()
    {
        foreach (AppConstants::CURRENCIES as $key => $value)
        {
            $currencyAmount = Redis::get($key.':amount');
            Account::create([
                'currency_id' => Currency::getIdByCode($key),
                'code' => $key,
                'status' => 'close',
                'amount' => Redis::get($key.':amount'),
                'amount_total' => Redis::get($key.':amount') + Currency::getCurrencySum($key), //10000 +  \App\Models\Wallet::where('currency_id', $currency->id)->sum('value') / 100,
                'day' => \Carbon\Carbon::today()->format('Y-m-d'),
            ]);
        }
        self::$status = FALSE;
    }


}