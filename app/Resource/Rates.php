<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 21.03.19
 * Time: 19:33
 */

namespace App\Resource;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class Rates
{
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    public static function getDefaultCurrency(): string
    {
        return config('app.main_currency', 'USD');
    }

    /**
     * @param string|null $date
     * @param string|null $currency
     * @return bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected static function ratesRequest(string $date = null, string $currency = null)
    {

        if (!$currency) {
            $currency = self::getDefaultCurrency();
        }
        try {
            $client = new Client();
            if ($date != null) {
                $URI = 'https://ratesapi.io/api/' . $date . '?base=' . $currency;
            } else {
                $URI = 'https://ratesapi.io/api/latest?base=' . $currency;
            }
            $response = $client->request('GET', $URI, []);
            return $response->getBody()->getContents();
        } catch (\Exception $ex) {
            \Log::error($ex->getMessage());
        }
        return FALSE;

    }

    /**
     * @param string|null $currency
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function reloadRates(string $currency = null): bool
    {
        $result = self::ratesRequest(Carbon::today()->format('Y-m-d'), $currency);
        $rates = json_decode($result, TRUE);
        if (json_last_error() == JSON_ERROR_NONE) {
            foreach ($rates['rates'] as $key => $rate) {
                Redis::set($key . ':rate', number_format($rate, 6, '.', ' '));
            }
            Redis::set('rates', $result);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param string|null $currency
     * @return mixed
     */
    public static function getRate(string $currency = null)
    {
        try {
            return Redis::get($currency);
        } catch (\Exception $ex) {
            \Log::error($ex->getMessage());
        }
    }

    public static function getAllRatesOnDate($date): array
    {
        $result = self::ratesRequest($date);
        $rates = json_decode($result, TRUE);
        if (json_last_error() == JSON_ERROR_NONE) {
            return $rates;
        }
        return [];
    }

    /**
     * @return array
     */
    public static function getAllRates(): array
    {
        try {
            $rates = json_decode(Redis::get('rates'), TRUE);
            if (json_last_error() == JSON_ERROR_NONE) {
                return $rates;
            }
        } catch (\Exception $ex) {
            \Log::error($ex->getMessage());
        }
        return [];
    }
}