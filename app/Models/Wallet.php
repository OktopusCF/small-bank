<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model
{
    use SoftDeletes;

    public $currencyCode;


    protected $fillable = [
        'user_id', 'currency_id', 'value', 'name', 'currency_code', 'wallet_no',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $appends = ['currency_code',];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }

    /**
     * @return mixed
     */
    public function getCurrencyCodeAttribute()
    {
        return $this->currency->code;
    }

    /**
     * @param $code
     */
    public function setCurrencyCodeAttribute($code)
    {
        $this->currencyCode = $code;
    }

    /**
     * @return mixed
     */
    public function getValueAttribute()
    {
        return $this->attributes['value'] / 100;
    }

    /**
     * @param $code
     */
    public function setValueAttribute($code)
    {
        $this->attributes['value'] = floor($code * 100);
    }

    /**
     * @param string $wallet_no
     * @return bool
     */
    public static function isWalletNoUnique(string $wallet_no): bool
    {
        return self::where('wallet_no', $wallet_no)->count() === 0;
    }

    public static function updateValue($wallet_no, $value): bool
    {
        return self::where('wallet_no', $wallet_no)->update(['value' => floor($value * 100)]);

    }

    /**
     * @param $wallet_no
     * @param $value
     * @return Model
     */
    public static function deposit($wallet_no, $value):Model
    {
        $wallet = self::where('wallet_no', $wallet_no)->first();
        $total = $wallet->value * 1 + $value * 1;
        $wallet->value = $total;
        $wallet->save();
        return $wallet;
    }

    /**
     * @param $wallet
     * @return bool
     */
    public static function isDefault(Wallet $wallet): bool
    {
        return self::find($wallet)->count() > 0;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public static function getUniqueWalletNo(): string
    {
        do {
            $unique = random_int(1000000000000000, 9999999999999999);
        } while (!self::isWalletNoUnique($unique));
        return strval($unique);
    }
}
