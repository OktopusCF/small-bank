<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = [
        'name', 'code',
    ];

    public $timestamps = false;

    /**
     * Get currency ID bu code
     * @param $code
     * @return int
     */
    public static function getIdByCode($code):int
    {
        return self::where('code', $code)->value('id');
    }

    /**
     * @param $code
     * @return int
     */
    public static function getCurrencySum($code):int
    {
        return Wallet::where('currency_id', self::getIdByCode($code))->sum('value') / 100;
    }

    /**
     * Get currency wallets
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function wallets()
    {
        return $this->hasMany('App\Models\Wallet');
    }
}
