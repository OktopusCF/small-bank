<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'api_token', 'password', 'country', 'city'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'wallets'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function wallets()
    {
        return $this->hasMany('App\Models\Wallet');
    }

    /**
     * @return mixed
     */
    public function getWalletsAttribute()
    {
        return $this->wallets()->getResults();
    }

    /**
     *
     */
    public function setWalletsAttribute()
    {

    }

    /**
     * @return mixed
     */
    public static function createDefaultWallet()
    {
        return Wallet::create([
            'currency_code' => config('app.main_currency'),
            'name' => "default"
        ]);
    }

    /**
     * @param $name
     * @return
     */
    public static function getUserByName($name)
    {
        return self::where('name', $name)->first();
    }
}
