<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'currency_id', 'code', 'status', 'amount', 'amount_total', 'day',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }


    /**
     * @return mixed
     */
    public function getAmountAttribute()
    {
        return $this->attributes['amount'] / 100;
    }

    /**
     * @param $code
     */
    public function setAmountAttribute($code)
    {
        $this->attributes['amount'] = floor($code * 100);
    }
    /**
     * @return mixed
     */
    public function getAmountTotalAttribute()
    {
        return $this->attributes['amount_total'] / 100;
    }

    /**
     * @param $code
     */
    public function setAmountTotalAttribute($code)
    {
        $this->attributes['amount_total'] = floor($code * 100);
    }
}
