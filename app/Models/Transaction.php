<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'donor_id',
        'acceptor_id',
        'donor_currency_id',
        'acceptor_currency_id',
        'donor_amount',
        'acceptor_amount',
        'main_currency_amount',
        'unique_hash',
        'description',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'deleted_at', 'updated_at',
    ];

    /**
     * @var array
     */
//    protected $appends = ['donor_data', 'acceptor_data', ];

    /**
     * @return mixed
     */
    public function getDonorAmountAttribute(): float
    {
        return floatval($this->attributes['donor_amount'] / 100);
    }

    /**
     * @param $amount
     */
    public function setDonorAmountAttribute($amount)
    {
        $this->attributes['donor_amount'] = floor($amount * 100);
    }

    /**
     * @return mixed
     */
    public function getMainCurrencyAmountAttribute(): float
    {
        return floatval($this->attributes['main_currency_amount'] / 100);
    }

    /**
     * @param $amount
     */
    public function setMainCurrencyAmountAttribute($amount)
    {
        $this->attributes['main_currency_amount'] = floor($amount * 100);
    }


    /**
     * @return mixed
     */
    public function getDonorDataAttribute()
    {
//        return $this->donor;
    }

    /**
     * @return mixed
     */
    public function getAcceptorDataAttribute()
    {
//        return $this->acceptor;
    }

    /**
     * @return mixed
     */
    public function getAcceptorAmountAttribute(): float
    {
        return floatval($this->attributes['acceptor_amount'] / 100);
    }

    /**
     * @param $amount
     */
    public function setAcceptorAmountAttribute($amount)
    {
        $this->attributes['acceptor_amount'] = floor($amount * 100);
    }

    public static function getExportTransactions($name, $wallet, $from = NULL, $to = NULL)
    {
        $user = User::getUserByName($name);
        $from = $from ? $from : \Carbon\Carbon::now()->startOfMonth();
        $to = $to ? $to : \Carbon\Carbon::tomorrow();
        $data = self::with('donor')->with('acceptor')
        ->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
        ->where(function ($q) use($wallet){
            $q->orWhereIn('donor_id', Wallet::where('wallet_no', $wallet)->pluck('id'));
            $q->orWhereIn('acceptor_id',  Wallet::where('wallet_no', $wallet)->pluck('id'));
        })->get();
        $res = $data->map(function($value)use($user){
            $result = [];
            $result['id'] = $value->id;
            $result['date'] = $value->created_at->format('Y-m-d H:i:s');
            if ($user->id === $value->donor_id){
                $result['wallet'] = strval($value->acceptor->wallet_no);
                $result['amount'] = number_format(floatval($value->donor_amount), 2);
                $result['amount_usd'] = $value->donor->currency_code == 'USD' ? number_format(floatval($value->donor_amount), 2)
                    : number_format(floatval($value->acceptor_amount), 2);
            }else{
                $result['wallet'] = strval($value->donor->wallet_no);
                $result['amount'] = number_format(floatval($value->acceptor_amount), 2);
                $result['amount_usd'] =  $value->acceptor->currency_code == 'USD' ? number_format(floatval($value->acceptor_amount), 2)
                    : number_format(floatval($value->donor_amount), 2);
            }
            return $result;
        });
        return $res;
    }

    /**
     * @param $name
     * @param $wallet
     * @param null $from
     * @param null $to
     * @return
     */
    public static function getTransactions($name, $wallet, $from = NULL, $to = NULL)
    {
        $user = User::getUserByName($name);
        $from = $from ? $from : \Carbon\Carbon::now()->startOfMonth();
        $to = $to ? $to : \Carbon\Carbon::tomorrow();
        return self::with('donor')->with('acceptor')
            ->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
            ->where(function ($q) use($wallet){
                $q->orWhereIn('donor_id', Wallet::where('wallet_no', $wallet)->pluck('id'));
                $q->orWhereIn('acceptor_id',  Wallet::where('wallet_no', $wallet)->pluck('id'));
            })->get();
    }

    /*************************************************/
    /*                   RELATIONS                   */
    /*************************************************/
    public function donor()
    {
        return $this->belongsTo('App\Models\Wallet', 'donor_id');
    }

    public function acceptor()
    {
        return $this->belongsTo('App\Models\Wallet', 'acceptor_id');
    }

    public function donorCurrency()
    {
        return $this->belongsTo('App\Models\Currency', 'donor_currency_id');
    }

    public function acceptorCurrency()
    {
        return $this->belongsTo('App\Models\Currency', 'acceptor_currency_id');
    }


}
