<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 22.03.19
 * Time: 13:10
 */

namespace App\Kernel;

use App\Models\Wallet;
use phpDocumentor\Reflection\Types\Mixed_;
use Redis;

class TransferOrder
{
    protected $donorWallet;
    protected $acceptorWallet;
    protected $amount;
    protected $donorAmount;
    protected $acceptorAmount;
    protected $is_acceptor_currency = FALSE;
    protected $convertRate;
    protected $defaultCurrency;
    protected $order = [];

    public $success = TRUE;
    public $message = "Ok";

    public function __construct($donorWallet, $acceptorWallet)
    {
        $this->donorWallet = Wallet::where('wallet_no', $donorWallet)->firstOrFail();
        $this->acceptorWallet = Wallet::where('wallet_no', $acceptorWallet)->firstOrFail();
        $this->defaultCurrency = config('app.main_currency');
        $this->convertRate = $this->convertRate($this->donorWallet->currency_code, $this->acceptorWallet->currency_code);
        $this->amount = 0;
        $this->order['description'] = 'Transfer of personal funds';
    }

    /**
     * @return array
     */
    public function create()
    {
        if ($this->donorHasEnoughSumm()) {
            $this->order['donor'] = $this->donorWallet->toArray();
            $this->order['donor']['user']['name'] = $this->donorWallet->user->name;
            $this->order['donor']['user']['email'] = $this->donorWallet->user->email;
            $this->order['acceptor'] = $this->acceptorWallet->toArray();
            $this->order['acceptor']['user']['name'] = $this->acceptorWallet->user->name;
            $this->order['acceptor']['user']['email'] = $this->acceptorWallet->user->email;
            $this->order['convert']['rate'] = $this->convertRate;
            $this->order['convert']['currency'] = $this->defaultCurrency;
            $this->order['convert']['amount'] = $this->getUsdFromCurrency($this->donorWallet->currency_code);
            $this->order['donor_amount'] = $this->donorAmount;
            $this->order['acceptor_amount'] = $this->acceptorAmount;
            $this->order['unique_hash'] = $this->createUniqueHash();
//            $this->order['description'] = '';
            $this->saveOrderToRedis($this->order);
            return $this->order;
        }
        $this->success = FALSE;
        $this->message = 'User has enough money on account';
        return false;
    }

    /**
     * @param array $order
     */
    protected function saveOrderToRedis(array $order)
    {
        Redis::set($order['unique_hash'], json_encode($order), 'EX', 600);
    }


    /**
     * @return bool
     */
    protected function donorHasEnoughSumm(): bool
    {
        return $this->donorWallet->value >= $this->donorAmount;
    }

    /**********************************/
    /*            HELPERS             */
    /**********************************/

    public function getUsdFromCurrency($code): float
    {
        $rate = $this->getRateFromRedis($code);
        return floor($rate / $this->donorAmount * 100) / 100;
    }

    public function toArray(): array
    {
        return $this->order;
    }

    /**
     * @return string
     */
    public static function createUniqueHash()
    {
        try {
            return bin2hex(random_bytes(40));
        } catch (\Exception $ex) {
            \Log::error($ex->getMessage());
            return self::createUniqueHash();
        }
    }

    /**
     * @param $code
     * @return bool|string
     */
    protected function getRateFromRedis($code)
    {
        return Redis::get($code . ':rate');
    }

    /**
     * @param $currency1
     * @param $currency2
     * @return float|int
     */
    protected function convertRate(string $currency1, string $currency2): float
    {
        $rate1 = $currency1 === $this->defaultCurrency ? 1 : $this->getRateFromRedis($currency1);
        $rate2 = $currency2 === $this->defaultCurrency ? 1 : $this->getRateFromRedis($currency2);
        return floor(((1 / $rate1) * $rate2) * 10000) / 10000;
    }

    /**********************************/
    /*       SETTERS AND GETTERS      */
    /**********************************/

    public function getDonorWallet()
    {
        return $this->donorWallet;
    }

    public function getAcceptorWallet()
    {
        return $this->acceptorWallet;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        $this->amount = floatval($amount);
        if ($this->is_acceptor_currency) {
            $this->donorAmount = floor($this->amount / $this->convertRate * 100) / 100;
            $this->acceptorAmount = $this->amount;
        } else {
            $this->acceptorAmount = floor($this->amount * $this->convertRate * 100) / 100;
            $this->donorAmount = $this->amount;

        }
        return $this;
    }
    public function setDescription($description)
    {
        $this->order['description'] = $description;
        return $this;
    }

    public function isAcceptorCurrency()
    {
        return $this->is_acceptor_currency;
    }

    /**
     * @param $is_acceptor_currency
     * @return $this
     */
    public function acceptorCurrency($is_acceptor_currency)
    {
        $this->is_acceptor_currency = $is_acceptor_currency;
        if ($this->amount > 0) {
            $this->setAmount($this->amount);
        }
        return $this;
    }
}