<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 22.03.19
 * Time: 16:16
 */

namespace App\Kernel;

use App\Exceptions\DonorHastEnoughMoney;
use App\Models\Wallet;
use Redis;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class TransferProcessor
{
    /**
     * order for transaction
     * @var mixed
     */
    protected $order;

    /**
     * TransferProcessor constructor.
     * @param null $order_hash
     */
    public function __construct($order_hash = null)
    {
        Redis::persist($order_hash);
        $this->order = json_decode(Redis::get($order_hash), TRUE);
        if (!($order_hash && $this->order && json_last_error() == JSON_ERROR_NONE)) {
            throw new NotFoundResourceException('Order not found');
        }
    }

    /**
     * @param $order_hash
     * @return $this
     */
    public function setHash($order_hash)
    {
        $this->order = json_decode(Redis::get($order_hash), TRUE);
        if ($this->order && json_last_error() == JSON_ERROR_NONE) {
            return $this;
        }
        throw new NotFoundResourceException('Order not found');
    }

    /**
     * @param string $code
     * @return bool|string
     */
    protected function getAccountAmountFromRedis(string $code)
    {
        return Redis::get($code . ':amount');
    }

    /**
     * @param string $code
     * @param float $amount
     * @return bool
     */
    protected function setAccountAmountToRedis(string $code, float $amount): bool
    {
        Redis::set($code . ':amount', $amount);
        return true;
    }

    /**
     * @param $amount
     * @return bool
     */
    protected function donorHasEnoughSumm($amount)
    {
        if ($this->order->donor_amount < $amount){
            throw new DonorHastEnoughMoney();
        }
    }

    /**
     * Making transaction for this order
     * @param string $type
     * @return bool
     * @throws \Exception
     */
    protected function transaction(string $type = 'donor'): bool
    {
        $account = $this->getAccountAmountFromRedis($this->order[$type]['currency_code']);
        $value = Wallet::where('wallet_no', $this->order[$type]['wallet_no'])->value('value');
        if ($type === 'donor') {
            $this->donorHasEnoughSumm($value);
            $newAmount = $account + $this->order[$type . '_amount'];
            $total = $value - $this->order[$type . '_amount'];
        } elseif ($type === 'acceptor') {
            $newAmount = $account - $this->order[$type . '_amount'];
            $total = $value + $this->order[$type . '_amount'];
        } else {
            throw new \Exception('Something wrong !!', 500);
        }
        $this->setAccountAmountToRedis($this->order[$type]['currency_code'], $newAmount);
        return Wallet::updateValue($this->order[$type]['wallet_no'], $total);
    }

    /**
     * Start transfer
     * @throws \Exception
     */
    public function transfer()
    {
        if ( !($this->transaction('donor') && $this->transaction('acceptor'))) {
            throw new \Exception('Something wrong!!!', 500);
        }
    }
}