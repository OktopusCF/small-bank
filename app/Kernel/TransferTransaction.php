<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 25.03.19
 * Time: 12:30
 */

namespace App\Kernel;


use Illuminate\Support\Facades\Log;
use Redis;
use App\Models\Wallet;
use App\Models\Transaction;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class TransferTransaction
{

    /**
     * order for transaction
     * @var mixed
     */
    protected $order;

    /**
     * TransferProcessor constructor.
     * @param string $order_hash
     */
    public function __construct(string $order_hash = null)
    {
        $this->order = json_decode(Redis::get($order_hash), TRUE);
        if (!($order_hash && $this->order && json_last_error() == JSON_ERROR_NONE)) {
            throw new NotFoundResourceException('Order not found');
        }
    }

    public function transfer()
    {
        Transaction::create([
            'donor_id' => Wallet::where('wallet_no', $this->order['donor']['wallet_no'])->value('id'),
            'acceptor_id' => Wallet::where('wallet_no', $this->order['acceptor']['wallet_no'])->value('id'),
            'donor_currency_id' => $this->order['donor']['currency']['id'],
            'acceptor_currency_id' => $this->order['acceptor']['currency']['id'],
            'donor_amount' => $this->order['donor_amount'],
            'acceptor_amount' => $this->order['acceptor_amount'],
            'main_currency_amount' => $this->order['main_currency_amount'],
            'unique_hash' => $this->order['unique_hash'],
            'status' => 'complete',
            'description' => $this->order['description'],
        ]);
    }

    public static function setErrorStatus(string $order_hash, string $message)
    {
        Transaction::where('unique_hash', $order_hash)->update([
            'status' => 'error',
            'description' => $message,
        ]);
        Log::error($message);
    }
}