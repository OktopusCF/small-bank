<?php

namespace App\Http\Controllers;


use App\Constants\HttpCodes;

class BaseController extends Controller
{
    /**
     * @param array $data
     * @param array $additionalData
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseWithOKStatus($data = [], $additionalData = [], $code = HttpCodes::SUCCESS)
    {
        if (gettype($additionalData) === 'array'){
            $data = array_merge($data, $additionalData);
        }else{
            $code = $additionalData;
        }
        return response()->json([
            'success' => true,
            'data' => $data,
        ], $code);
    }

    /**
     * @param $messages
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseWithError($messages, $code = HttpCodes::BAD_REQUEST)
    {
        return response()->json([
            'success' => false,
            'message' => [$messages],
        ], $code);
    }
}
