<?php

namespace App\Http\Controllers;

use App\Constants\HttpCodes;
use App\Constants\HttpMessages;
use App\Http\Requests\WalletRequest;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WalletController extends BaseController
{
    public function index()
    {
        $wallets = auth()->user()->wallets;
        return $this->responseWithOKStatus(['wallets'=> $wallets->toArray()], [], HttpCodes::CREATED);
    }

     /**
     * Store a newly created resource in storage.
     * @param WalletRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(WalletRequest $request)
    {
        $wallet = Wallet::create($request->all());
        return $this->responseWithOKStatus(['wallet'=> $wallet->toArray()], [], HttpCodes::CREATED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Wallet $wallet
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Wallet $wallet)
    {
        if (!in_array($wallet, auth()->user()->wallets->all())){
            return $this->responseWithError(HttpMessages::FORBIDDEN, HttpCodes::FORBIDDEN);
        }
        if ($wallet->value !== 0 ){
            return $this->responseWithError(HttpMessages::BAD_REQUEST." Wallet can't be delete. It's not empty.", HttpCodes::BAD_REQUEST);
        }
        if ($wallet->delete()){
            return $this->responseWithOKStatus([],[], HttpCodes::SUCCESS);
        }
    }
}
