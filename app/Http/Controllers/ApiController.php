<?php

namespace App\Http\Controllers;

use App\Exports\TransactionExport;
use App\Jobs\ProcessorJob;
use App\Jobs\TransactionJob;
use App\Kernel\TransferOrder;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Resource\Rates;
use Carbon\Carbon;
use Excel;
use Redis;
use Illuminate\Support\Facades\Validator;

class ApiController extends BaseController
{
    public function __construct()
    {
        $this->middleware('api')->except('deposit');
    }

    /**
     * @param $wallet_no
     * @return \Illuminate\Http\JsonResponse
     */
    public function deposit($wallet_no)
    {
        if ($wallet = Wallet::deposit($wallet_no, request()->input('value'))) {
            $inUSD = floor((request()->input('value') * 1) / Redis::get($wallet->currency_code . ':rate') * 100) / 100;
            $rate = Redis::get($wallet->currency_code . ':rate');
            $data = [
                'donor_id' => 1,
                'acceptor_id' => $wallet->id,
                'donor_currency_id' => $wallet->currency_id,
                'acceptor_currency_id' => $wallet->currency_id,
                'donor_amount' => request()->input('value'),
                'acceptor_amount' => request()->input('value'),
                'main_currency_amount' => $inUSD,
                'unique_hash' => TransferOrder::createUniqueHash(),
                'status' => 'complete',
                'description' => 'Donate',
            ];
//            dd('data', $data, Redis::get($wallet->currency_code . ':rate'), request()->input('value') * 1, $inUSD, $inUSD * Redis::get($wallet->currency_code . ':rate'));
            Transaction::create($data);
            return $this->responseWithOKStatus();
        } else {
            return $this->responseWithError(['message' => 'Error. Wallet not replenished'], 200);
        }
    }

    /**
     *
     */
    public function getRates($date = null)
    {
        return $this->responseWithOKStatus(['rates' => Rates::getAllRatesOnDate($date ? $date : Carbon::today()->format('Y-m-d'))]);
    }

    public function exportTransaction($name, $wallet, $from = null, $to = null)
    {
        return Excel::download(new TransactionExport($name, $wallet, $from, $to), 'transaction.xlsx');
    }

    /**
     *
     */
    public function transferStepOne()
    {
        $data = request()->all();
        $rules = $this->getValidationRulesStepOne();
        Validator::make($data, $rules)->validate();
        $order = new TransferOrder($data['donor_wallet_no'], $data['acceptor_wallet_no']);
        if (trim($data['description']) !=='') $order->setDescription($data['description']);
        $order->setAmount($data['amount'])->acceptorCurrency($data['is_acceptor_currency'])->create();
        if ($order->success)
            return $this->responseWithOKStatus($order->toArray(), 201);
        else
            return $this->responseWithError($order->message, 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function transferStepTwo()
    {
        $data = request()->all();
        Validator::make($data, ['unique_hash' => 'required|string|filled'])->validate();
        TransactionJob::withChain([(new ProcessorJob($data['unique_hash']))->onQueue('high')])->dispatch($data['unique_hash'])->onQueue('low');
        return $this->responseWithOKStatus(['message' => 'Transfer in process. Please reload page after few minutes']);
    }

    public function getTransaction($name, $wallet, $from = null, $to = null)
    {
        return $this->responseWithOKStatus(['data' => Transaction::getTransactions($name, $wallet, $from, $to)]);
    }

    /**
     * @return array
     */
    protected function getValidationRulesStepOne(): array
    {
        return [
            'donor_wallet_no' => "required|string",
            'acceptor_wallet_no' => "required|string|different:donor_wallet_no",
            'is_acceptor_currency' => "filled|boolean",
            'amount' => "required|numeric",
            'description' => "filled",
        ];
    }
}
