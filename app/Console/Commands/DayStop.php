<?php

namespace App\Console\Commands;

use App\Resource\Day;
use Illuminate\Console\Command;

class dayStop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'day:stop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stop transaction time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Day::stop();
    }
}
