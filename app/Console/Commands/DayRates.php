<?php

namespace App\Console\Commands;

use App\Resource\Rates;
use Illuminate\Console\Command;

class DayRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'day:getRate 
                            {currency? : Three letters currency code}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get exchange rates';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if ($result = Rates::reloadRates()) {
            echo "DONE!";
        } else {
            echo "Something wrong! Try later";
        }
    }
}
