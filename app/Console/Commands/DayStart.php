<?php

namespace App\Console\Commands;

use App\Resource\Day;
use App\Resource\Rates;
use Illuminate\Console\Command;

class DayStart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'day:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start transaction time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        Rates::reloadRates();
        Day::start();
    }
}
