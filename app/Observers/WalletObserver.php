<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 20.03.19
 * Time: 4:22
 */

namespace App\Observers;


use App\Models\Currency;
use App\Models\Wallet;
use Log;

class WalletObserver
{
    public function creating(Wallet $wallet)
    {
        try{
            $wallet->user_id = auth()->id();
            $wallet->currency_id = Currency::where('code', $wallet->currencyCode)->value('id');
            $wallet->wallet_no = Wallet::getUniqueWalletNo();
            $wallet->value = 0;
            return $wallet;
        }catch (\Exception $ex){
            Log::error($ex->getMessage());
        }
    }
}