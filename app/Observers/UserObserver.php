<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 20.03.19
 * Time: 4:11
 */

namespace App\Observers;

use App\Models\User;

class UserObserver
{
    public function creating(User $user)
    {
        $user->password = bcrypt($user->password);
        $user->api_token = sha1(mt_rand(1, 90000) . 'SALT');
        return $user;
    }

    public function created(User $user)
    {
        auth()->login($user);
        $user->createDefaultWallet();
    }
}