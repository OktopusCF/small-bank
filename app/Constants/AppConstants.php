<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 24.03.19
 * Time: 7:38
 */

namespace App\Constants;


class AppConstants
{
    const CURRENCIES = [
        "AUD" => "Australian Dollar",
        "BGN" => "Bulgarian lev",
        "BRL" => "Brazilian real",
        "CAD" => "Canadian dollar",
        "CHF" => "Swiss franc",
        "CNY" => "Chinese yuan",
        "CZK" => "Czech koruna",
        "DKK" => "Danish krone",
        "EUR" => "Euro",
        "GBP" => "British pound",
        "HKD" => "Hong Kong dollar",
        "HRK" => "Croatian kuna",
        "HUF" => "Hungarian forint",
        "IDR" => "Indonesian rupiah",
        "ILS" => "Israeli new shekel",
        "INR" => "Indian rupee",
        "ISK" => "Icelandic króna",
        "JPY" => "Japanese yen",
        "KRW" => "South Korean won",
        "MXN" => "Mexican peso",
        "MYR" => "Malaysian ringgit",
        "NOK" => "Norwegian krone",
        "NZD" => "New Zealand dollar",
        "PHP" => "Philippine peso",
        "PLN" => "Polish złoty",
        "RON" => "Romanian leu",
        "RUB" => "Russian ruble",
        "SEK" => "Swedish krona",
        "SGD" => "Singapore dollar",
        "THB" => "Thai baht",
        "TRY" => "Turkish lira",
        "USD" => "United State Dollar",
        "ZAR" => "South African rand"
    ];
}