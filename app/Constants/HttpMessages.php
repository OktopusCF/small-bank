<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 21.03.19
 * Time: 17:52
 */

namespace App\Constants;

class HttpMessages
{
    const SUCCESS = 'Ok';
    const CREATED = 'Created';

    const BAD_REQUEST = 'Bad request';
    const UNAUTHORIZED = 'Unauthorized';
    const FORBIDDEN = 'Forbidden';
    const NOT_FOUND = 'Not found';
    const UNPROCESSABLE_ENTITY = 'Unprocessable Entity';

    const SERVER_ERROR = 'Internal Server Error';

}