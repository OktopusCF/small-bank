<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 21.03.19
 * Time: 17:52
 */

namespace App\Constants;

class HttpCodes {
    const SUCCESS = 200;
    const CREATED = 201;

    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const UNPROCESSABLE_ENTITY = 422;

    const SERVER_ERROR = 500;

}