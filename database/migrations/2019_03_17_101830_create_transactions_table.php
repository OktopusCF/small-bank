<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('donor_id')->unsigned();
            $table->integer('acceptor_id')->unsigned();
            $table->integer('donor_currency_id')->unsigned();
            $table->integer('acceptor_currency_id')->unsigned();
            $table->bigInteger('donor_amount')->unsigned();
            $table->bigInteger('acceptor_amount')->unsigned();
            $table->bigInteger('main_currency_amount')->unsigned();
            $table->string('unique_hash', 100)->unique();
            $table->string('status')->default('complete');
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('donor_id')->references('id')->on('wallets');
            $table->foreign('acceptor_id')->references('id')->on('wallets');
            $table->foreign('donor_currency_id')->references('id')->on('currencies');
            $table->foreign('acceptor_currency_id')->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
