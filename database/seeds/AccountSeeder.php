<?php

use App\Models\Account;
use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $wallets = \App\Models\Wallet::get();
        foreach ($wallets as $wallet){
            $wallet->value = random_int(10, 1000);
            $wallet->save();
        }
        $currencies = \App\Models\Currency::get();
        foreach ($currencies as $currency){
            Account::create([
                'code' => $currency->code,
                'currency_id' => $currency->id,
                'status' => 'close',
                'amount'  => 10000,
                'amount_total' => 10000 +  \App\Models\Wallet::where('currency_id', $currency->id)->sum('value') / 100,
                'day' => \Carbon\Carbon::yesterday()->format('Y-m-d'),
                ]);
        }
    }
}
