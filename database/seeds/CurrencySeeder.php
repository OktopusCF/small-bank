<?php

use App\Constants\AppConstants;
use App\Models\Currency;
use App\Models\Account;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (AppConstants::CURRENCIES as $key=>$value){
            Currency::create([
                'name' => $value,
                'code' => $key
            ]);
        }
    }
}
