<?php

use App\Models\Wallet;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'donator',
            'email' => 'donator@mail.com',
            'country' => 'Ukraine',
            'city' => 'Kyiv',
            'password' => 'secret',
        ]);

        \App\Models\User::create([
            'name' => 'admin',
            'email' => 'admin@mail.com',
            'country' => 'Ukraine',
            'city' => 'Kyiv',
            'password' => 'secret',
        ]);

        \App\Models\User::create([
            'name' => 'superuser',
            'email' => 'superuser@mail.com',
            'country' => 'Ukraine',
            'city' => 'Kyiv',
            'password' => 'secret',
        ]);


        \App\Models\User::create([
            'name' => 'user',
            'email' => 'user@mail.com',
            'country' => 'Ukraine',
            'city' => 'Kyiv',
            'password' => 'secret',
        ]);
    }
}
